**COMP 7881**
*Advanced Topis in Software Engineering (DevOps)*

This repo was created to test the GitLa Tool as a Version Control tool.
---

## Steps

1. Create GitLab Account
   Note I used my existing GitHub account and linked the 2
2. Cloud based so nothing to install
   *No need for seperate issue tracking Software as GitLab does both*
3. Create Repository
   Group Name = COMP_7881
   Repo Name = assign_1
   *Can use preexisting repository templates that include the necessary files to get started with various project types such as Rails MVC, Jave Spring etc*
4. Added issues to GitLab
5. Linked VSCode to GitLab to test integrations
6. Create a branch
7. Created new file (sample_file.txt)
8. Update README.md file to include purpose and steps
9. Commit the changes into the branch
10. Merge the branch into Main Branch 

---